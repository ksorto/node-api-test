require('dotenv').config()
const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const routes = require('./src/routes/index')

const app = express()

app.use(helmet())
app.use(morgan('combined'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())


app.use('/api', routes)



app.listen(process.env.APP_PORT || 3000)

