const users = require('./../data/users')
const router = require('express').Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

router.get('/', async (request, response) => {
    const { page_size, current_page } = request.query
    const results =  await users.getUsers(page_size, current_page)
    response.status(200).json({
        status: "success",
        data: results
    })
})

router.get('/:id', async (request, response) => {
    const user = await users.getUserById(request.params.id)
    if(!user) {
        return response.status(404).json({
            status: "failed",
            message: "user not found"
        })
    }
    response.status(200).json({
        status: "success",
        data: user
    })
})

router.post('/', async (request, response) => {
    const { name, email, password, country_id } = request.body
    // Validate if the user email already exists
    const emailRegistered = await users.getUserByEmail(email)
    if(emailRegistered) {
        return response.status(409).json({
            status: "failed",
            message: " user already exists"
        })
    }
    const newPassword = await bcrypt.hash(password, 10)
    const user = await users.createUser({
        name, email, password: newPassword, country_id
    })

    const token = await jwt.sign({user_id: user.id, email}, process.env.JWT_TOKEN, { expiresIn: "12h"})
    response.status(201).json({
        status: "success",
        message: "user created",
        data: {...user, token}
    })
})

router.put('/:id', async (request, response) => {
    const { name, email, password, country_id } = request.body
    const id = request.params.id
    await users.updateUser(id, name, email, country_id)
    const user = await users.getUserById(id)
    response.status(200).json({
        status: "success",
        message: "user updated",
        data: user
    })
})

router.delete('/:id', async (request, response) => {
    const id = request.params.id
    await users.deleteUser(id)
    response.status(200).json({
        status: "success",
        message: "user deleted"
    })
})

module.exports = router