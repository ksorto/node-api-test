const router = require('express').Router()
const users = require('./users')
const countries = require('./countries')
const authMiddleware = require('./../middleware/auth')

router.use('/users', authMiddleware)
router.use('/users', users)

router.use('/countries', authMiddleware)
router.use('/countries', countries)

router.get("/", function (request, response) {
    response.status(200).json({
        message: "API running"
    })
})

module.exports = router