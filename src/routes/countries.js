const router = require('express').Router()
const countries = require('./../data/countries')

router.get('/', async (request, response) => {
    const { page_size, current_page } = request.query
    response.status(200).json({
        data: await countries.getCountries(page_size, current_page)
    })
})

router.get('/:id', async (request, response) => {
    const id = request.params.id
    const country = await countries.getCountryById(id)
    if(!country) {
        return response.status(404).json({
            status: "failed",
            message: "country not found"
        })
    }
    response.status(200).json({
        status: "success",
        data: country
    })
})

router.post("/", async (request, response) => {
    console.log(request.body)
    const name = request.body.name
    const country = await countries.createCountry(name)
    response.status(201).json({
        status: "success",
        message: "user created",
        data: country
    })
})

router.put("/:id", async (request, response) => {
    const name = request.body.name
    const id = request.params.id
    await countries.updateCountry(id, name)
    const country = await countries.getCountryById(id)
    response.status(200).json({
        status: "success",
        message: "user updated",
        data: country
    })
})

router.delete("/:id", async (request, response) => {
    const id = request.params.id
    await countries.deleteCountry(id)
    response.status(200).json({
        status: "success",
        message: "user deleted"
    })
})

module.exports = router