const jwt = require('jsonwebtoken')

const verifyToken = (request, response, next) => {
    if((request.originalUrl === '/api/countries' || request.originalUrl === '/api/users') && request.method === 'POST') {
        return next()
    }
    const authorization = request.headers['authorization']
    if(!authorization) {
        return response.status(403).send('unauthenticated')
    }
    const bearerTokenSplit = authorization.split(" ")
    const token = bearerTokenSplit[1]
    try  {
        request.user = jwt.verify(token, process.env.JWT_TOKEN)
    } catch(err) {
        return response.status(401).send("invalid token")
    }

    return next();
}

module.exports = verifyToken