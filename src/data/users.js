const pool = require('./db')

const createUser = async ({name, email, password, country_id}) => {
    const result = await pool.query("INSERT INTO users (name, email, password, country_id) VALUES($1, $2, $3, $4) RETURNING id", [name, email, password, country_id])

    const userId = result.rows[0].id
    return await getUserById(userId)
}

const getUserById = async (id) => {
    const result = await pool.query("SELECT users.*, countries.name FROM  users JOIN countries on countries.id = users.country_id WHERE users.active is true AND users.id = $1", [id])
    return result.rows[0];
}

const getUserByEmail = async (email) => {
    const result = await pool.query("SELECT * FROM users WHERE active is true AND email = $1", [email])
    return result.rows[0] || null
}

const getUsers  = async (page_size = 5, current_page = 1) => {
    const offset = page_size * (current_page - 1)
    const result = await pool.query("SELECT users.*, countries.name FROM users INNER JOIN countries on countries.id = users.country_id WHERE users.active is true OFFSET $1 LIMIT $2", [offset, page_size])
    return result.rows
}

const updateUser = async (id, name, email, country_id) => {
    await pool.query("UPDATE users SET  name = $1, email = $2, country_id = $3 WHERE id = $4", [name, email, country_id, id]);
}

const deleteUser = async (id) => {
    await pool.query("UPDATE users SET active = false WHERE id = $1", [id])
}

module.exports = {
    createUser,
    deleteUser,
    getUsers,
    getUserById,
    getUserByEmail,
    updateUser
}