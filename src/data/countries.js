const pool = require('./db')

const createCountry = async ( name ) => {
    const result = await pool.query('INSERT INTO countries (name) VALUES ($1) RETURNING id', [name])
    const countryId = result.rows[0].id
    return await getCountryById(countryId)
}

const getCountryById = async (id) => {
    const result = await pool.query("SELECT * FROM countries WHERE id = $1 AND active is true", [id])
    return result.rows[0]
}

const getCountries = async (page_size = 5, current_page = 1) => {
    const offset = page_size * (current_page - 1)
    const result = await pool.query('SELECT * FROM countries WHERE active is true OFFSET $1 LIMIT $2', [offset, page_size])
    return result.rows
}

const updateCountry = async (id, name) => {
    await pool.query("UPDATE countries SET name = $1 WHERE id = $2", [name, id])
}

const deleteCountry = async (id) => {
    await pool.query("UPDATE countries SET active = false WHERE id = $1", [id])
}

module.exports = {
    createCountry,
    getCountryById,
    getCountries,
    deleteCountry,
    updateCountry
}